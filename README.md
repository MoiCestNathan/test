# Inside

Une application de retransmission de concert multipoints en direct.

## Utilisation de l'application
---

L'application permet de retransmettre le flux vidéo de la caméra en direct sur le page Facebook de l'application Inside.

A l'ouverture de l'application, des champs sont à remplir pour fournir les informations du concert. On peut ensuite soit créer un concert ou rejoindre un concert existant.

Si le concert a bien pu être créé/rejoint, on passe à la vue concert. Il y a une preview de la caméra et la possibilité de démarrer le concert. Les vidéos des différents point du vue des téléphones connectés au concert sont ensuites mélangées et diffusées sur Facebook.

Le concert peut être démarré et arrêté par la personne l'ayant créé.

## Installation de l'environnement de développement
---
L'application est basée sur la technologie dart/flutter.
Pour installer l'environnement il faut suivre le tutoriel sur le site de flutter:
[https://docs.flutter.dev/get-started/install](https://docs.flutter.dev/get-started/install)

Il faut ensuite exécuter la commande ``flutter pub get`` à la racine du projet pour télécharger toutes les dépendances.

Il faut également définir l'url vers l'API dans le fichier ``communication\lib\src\communication.dart``

## Fonctionnement de l'application
---

Le point d'entrée du programme est le fichier ``lib/main.dart``

Chaque élément de chaque page est séparé dans son fichier. 
Tous les fichiers sont situés dans le dossier ``lib`` à la racine du projet.

A la création d'un concert, utilise l'API HTTP Inside pour créer un concert côté serveur et récupérer le cookie. Ensuite le cookie est utilisé pour initialiser la connexion webSockets avec le serveur.

La librairie ``Communication`` permet de communiquer avec le serveur. Chaque message reçu par socket est associé à une action.
Cette librairie est située dans le dossier ``communication`` à la racine du projet.

Le retour au menu principal se fait par un Restart de l'application pour revenir à un état initial.

Pour suivre un chemin d'utilisation, il faut regarder la fonction click() du widget sur lequel on clique.