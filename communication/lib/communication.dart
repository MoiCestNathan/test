/// Librairie pour communiquer avec le serveur Inside
library communication;

export 'src/cookie_storage.dart';
export 'src/communication.dart';
