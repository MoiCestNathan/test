import 'dart:io';

import 'package:path_provider/path_provider.dart';

///Permet de gérer la sauvegarde et récupération d'un cookie
class CookieStorage {
  ///Retourne le chemin vers le dossier de l'application
  ///
  ///Utiliser 'await' pour pouvoir utiliser la réponse juste après
  static Future<String> get localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  ///Retourne le dossier de l'application
  ///
  ///Utiliser 'await' pour pouvoir utiliser la réponse juste après
  static Future<Directory> get localDirectory async {
    final directory = await getApplicationDocumentsDirectory();

    return directory;
  }

  ///Le nom du fichier dans lequel stocker le cookie
  static String filename = 'cookie.txt';

  ///Stocke un cookie dans un fichier
  ///
  ///[cookie] est le cookie à stocker
  static Future<void> storeCookie(String cookie) async {
    //Fichier
    final file = File(await localPath + '/' + filename);
    //Ecrit le cookie dans le fichier
    file.writeAsStringSync(cookie);
    print('Storing cookie:' + cookie);
  }

  ///Récupère un cookie sauvegardé
  static Future<String> getStoredCookie() async {
    try {
      final file = File(await localPath + '/' + filename);

      // Read the file
      final cookie = await file.readAsString();

      return cookie;
    } catch (e) {
      print(e);
      return null;
    }
  }
}
