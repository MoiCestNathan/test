//import 'dart:html';
import 'dart:io';

import 'package:dio/dio.dart';

import 'package:socket_io_client/socket_io_client.dart' as io;
import 'package:socket_io_client/socket_io_client.dart';

///Permet la communication avec l'API Inside
class Communication {
  ///Cookie pour l'authentification avec le serveur
  static var cookie = '';

  ///Objet permettant d'envoyer des requêtes http
  static Dio dio = Dio();

  ///Objet permettant de communiquer en websocket avec le serveur
  static Socket socket;

  ///URL vers la racine du serveur http
  static String urlAPI = 'http://192.0.0.4:3000';

  ///URL vers le serveur websocket
  static String urlSocketIO = 'ws://192.0.0.4:3000';

  /// Crée un concert
  ///
  /// Stocke le cookie renvoyé par le serveur si succès
  ///
  /// Retour 0 si succès, 1 si erreur
  static Future<int> creerConcert(
      String titre, String description, String mdp) async {
    var formData = FormData.fromMap(
        {'titre': titre, 'description': description, 'password': mdp});
    print("Create - 1");
    var response =
        await dio.post(urlAPI + '/api/concert/create', data: formData);
    print(response);
    if (response.data['status']) {
      cookie = response.data['cookie'];
      return 0;
    } else {
      print(response.data['message']);
      return 1;
    }
  }

  ///Permet de rejoindre un concert
  ///
  ///Demande un cookie au serveur pour pouvoir se connecter via websocket
  ///
  ///Stocke le cookie si succès
  ///
  /// Retour 0 si succès, 1 si erreur
  static Future<int> rejoindreConcert(String titre, String mdp) async {
    var formData =
        FormData.fromMap({'titre': titre, 'password': mdp, 'cookie': cookie});
    var response = await dio.post(urlAPI + '/api/concert/join', data: formData);

    if (response.data['status']) {
      cookie = response.data['cookie'];
      return 0;
    } else {
      print(response.data['message']);
      return 1;
    }
  }

  ///Télécharge une vidéo vers le serveur Inside
  ///
  ///Utilise le cookie sauvegardé lors de la connexion ou création du concert
  ///
  ///[path] chemin vers le fichier de la vidéo à envoyer
  ///
  /// Retour 0 si succès, 1 si erreur
  static Future<int> uploadVideo(String path) async {
    print('chargement de la video ' + path);
    var formData = FormData.fromMap({
      'cookie': cookie,
      'video': await MultipartFile.fromFile(path),
    });
    print('envoi de la video ' + path);
    var response =
        await dio.post(urlAPI + '/api/concert/video', data: formData);
    print('suppression de la video ' + path);
    
    var file = File(path);
    file.deleteSync();
    if (response.data['status']) {
      return 0;
    } else {
      print(response.data['message']);
      return 1;
    }
  }

  ///Met fin au concert
  ///
  ///Utilise le cookie sauvegardé lors de la connexion ou création du concert
  ///
  /// Retour 0 si succès, 1 si erreur
  static Future<int> terminerConcert() async {
    var formData = FormData.fromMap({
      'cookie': cookie,
    });
    var response = await dio.post(urlAPI + '/api/concert/end', data: formData);

    socket?.disconnect();

    if (response.data['status']) {
      cookie = '';
      return 0;
    } else {
      print(response.data['message']);
      return 1;
    }
  }

  /// Initialise la connexion websocket avec le serveur
  ///
  /// Rejoint un concert via websockets
  ///
  /// [callback] est la fonction à exécuter une fois le concert rejoint
  /// [startRecording] est la fonction à exécuter lorsque le concert démarre
  /// [checkPointVideo] est la fonction à exécuter lorsque le serveur indique qu'il faut enregistrer et envoyer la vidéo
  /// [endConcert] est la fonction à exécuter lorsque le concert se termine
  ///
  /// Retourne 0 si la connexion a été établie, 1 sinon.
  static bool setupSockets(Function callback, Function startRecording,
      Function checkPointVideo, Function endConcert) {
    print('Connexion au serveur SocketIO ' + urlSocketIO);
    socket = io.io(
        urlSocketIO,
        OptionBuilder()
            .setTransports(['websocket']) // for Flutter or Dart VM
            .disableAutoConnect() // disable auto-connection
            .build());
    socket.connect();

    socket.onConnect((data) {
      print('socket connecté');
      socket.emit('rejoindre_concert', {'cookie': cookie});
    });

    socket.on('concert_rejoint', (data) {
      print('socket reçu: concert_rejoint');
      callback();
    });

    socket.on('demarrer_concert', (data) {
      //print(data);
      print('socket reçu: demarrer_concert');

      //Lancer l'enregistrement de la vidéo
      startRecording();
    });

    socket.on('fin_concert', (data) {
      //print(data);
      print('socket reçu: fin_concert');
      endConcert();
    });

    socket.on('checkpoint_video', (data) {
      print('socket reçu: checkpoint_video');

      checkPointVideo();
    });

    socket.onDisconnect((data) {
      print('socket déconnecté');
      endConcert();
    });

    return !socket.connected;
  }

  ///Lance le concert
  ///
  ///Utilise le cookie sauvegardé lors de la connexion ou création du concert
  static void demarrerConcert() {
    socket?.emit('demarrer_concert', {'cookie': cookie});
  }
}
