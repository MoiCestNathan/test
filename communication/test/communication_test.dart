import 'package:communication/communication.dart';
import 'package:test/test.dart';

void main() {
  group('A group of tests', () {
    //CookieStorage cookie;

    setUp(() {
      //cookie = CookieStorage();
    });

    test('First Test', () {
      var cookie = 'test';

      CookieStorage.storeCookie(cookie).then((x) =>
          CookieStorage.getStoredCookie()
              .then((value) => expect(value, cookie)));
    });
  });
}
