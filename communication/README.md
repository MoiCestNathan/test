Une librairie pour communiquer avec le serveur Inside

## Utilisation
---
```dart
import 'package:communication/communication.dart';

main() {

    Communication.creerConcert(nom, description, mdp)
    .then((value) {
      print('Valeur de retour:' + value.toString());
      if(value == 0){
        //On a créé le concert et on a récupéré le cookie principal

        //Initialisation de la connexion websocket
        Communication.setupSockets(
        (){ //Callback
        print('passage vers la vue concert');
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: routes.values.elementAt(1),
                settings: RouteSettings(arguments: concertModel, name: '/concert')),
          );
        },
        (){
          //startRecording

          globals.cameraController.startVideoRecording();
        }, 
        (){

          //checkPointVideo
          globals.cameraController?.stopVideoRecording()?.then((file) {
            Communication.uploadVideo(file.path);
            globals.cameraController.startVideoRecording();
          });

        }, 
        (){

          //endConcert
          globals.cameraController?.stopVideoRecording()?.then((file) {
            Communication.uploadVideo(file.path);
          });
          
        }
        );
}
```
Il existe une fonction pour interagir avec chaque URL de l'API Inside.

Fonctions:
```dart
static Future<int> creerConcert(String titre, String description, String mdp) async
static Future<int> rejoindreConcert(String titre, String mdp) async 
static Future<int> uploadVideo(String path) async 
static Future<int> terminerConcert() async
static bool setupSockets(Function callback, Function startRecording,Function checkPointVideo, Function endConcert)
static void demarrerConcert()
```

Pour une description détaillée de l'utilisation de chaque fonction, regarder la documentation dartDoc.


## Références
---
[dio](https://pub.dev/packages/dio): Librairie pour la communication HTTP  
[socket_io](https://pub.dev/packages/socket_io_client): Librairie pour la communication websockets

