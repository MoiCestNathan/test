import 'package:flutter/material.dart';
import 'package:inside/models/cameras.dart' as globals;
import 'package:restart_app/restart_app.dart';
import '../../../models/concert.dart';
import '../../../routes.dart';
import 'package:communication/communication.dart';

///Bouton pour créer un concert
class CreerConcertButton extends StatelessWidget {
  ConcertModel concertModel;

  CreerConcertButton(ConcertModel concertModel) {
    this.concertModel = concertModel;
  }

  onClick(BuildContext context) {
    print(concertModel.nom);
    print(concertModel.mdp);
    print(concertModel.description);

    Communication.creerConcert(
            concertModel.nom, concertModel.description, concertModel.mdp)
        .then((value) {
      print('Valeur de retour:' + value.toString());
      if (value == 0) {
        //On a créé le concert et on a récupéré le cookie principal

        //Initialisation de la connexion websocket
        Communication.setupSockets(() {
          //Callback
          print('passage vers la vue concert');
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: routes.values.elementAt(1),
                settings:
                    RouteSettings(arguments: concertModel, name: '/concert')),
          );
        }, () {
          //startRecording

          globals.cameraController.startVideoRecording();
        }, () {
          //checkPointVideo
          try{
            print('arrêt de la vidéo');
            globals.cameraController?.stopVideoRecording()?.then((file) {
              print('envoi de la vidéo: ' + file.path);
              Communication.uploadVideo(file.path);
              globals.cameraController.startVideoRecording();
            });
          }catch(err){
            print('erreur lors de l arret de la video');
            print(err);
              globals.cameraController.startVideoRecording();
          }


        }, 
            () {
              //endConcert
              
          try{
            globals.cameraController?.stopVideoRecording()?.then((file) {
             // Communication.uploadVideo(file.path);
            });
          }catch(err){

          }
          
          Restart.restartApp(webOrigin: '/mainMenu');
        } 
            );
      }else{
        //TODO: Afficher un pop-up d'erreur à la création du concert
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      //définition de la taille et du style du container principal
      margin: EdgeInsets.all(5),
      width: MediaQuery.of(context).size.width * 0.25,
      height: MediaQuery.of(context).size.height * 0.20,
      decoration: BoxDecoration(
          border: Border.all(color: Color.fromRGBO(32, 25, 90, 1)),
          borderRadius: BorderRadius.all(Radius.circular(20)),
          boxShadow: [
            BoxShadow(
              spreadRadius: 1.5,
              offset: new Offset(2, 2),
              color: Color.fromRGBO(0, 0, 0, 0.75),
            ),
          ],
          image: DecorationImage(
            image: AssetImage("assets/texture-simple.png"),
            fit: BoxFit.cover,
          )),
      //bouton permettant d'afficher la fenêtre modale de modification
      child: TextButton(
        style: TextButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
        ),
        //fonction appelée lors de la pression du bouton, cette fonction va ouvrir une fenêtre modale
        onPressed: () {
          onClick(context);
        },
        child: Align(
          child: Text('Creer un \n concert',
              style: TextStyle(
                fontSize: MediaQuery.of(context).size.height * 0.05,
                color: Colors.white,
                fontFamily: 'Corps',
              ),
              textAlign: TextAlign.center),
        ),
      ),
    );
  }
}
