//import 'dart:ffi';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:inside/models/concert.dart';

//import '../../routes.dart';
import 'components/creerConcertButton.dart';
import 'components/rejoindreConcertButton.dart';
import 'package:inside/models/cameras.dart' as globals;

import 'package:wakelock/wakelock.dart';


///Menu principal, création/joignage d'un concert
class MainMenu extends StatefulWidget {
  @override
  _MainMenuState createState() => _MainMenuState();
}

class _MainMenuState extends State<MainMenu> {
  // Instance du concert
  ConcertModel concertModel;
  bool _passwordVisible = false;
  final titleController = TextEditingController();
  final passwordController = TextEditingController();
  final descController = TextEditingController();

  updateNom(String nom) {
    this.concertModel.nom = nom;
  }

  updateMdp(String mdp) {
    this.concertModel.mdp = mdp;
  }

  updateDesc(String desc) {
    this.concertModel.description = desc;
  }

  @override
  Future<void> initState() {
      //Attendre la fin de l'initialisation des widgets
  WidgetsFlutterBinding.ensureInitialized();
  Wakelock.enable();
  //Recherche des cameras
  availableCameras().then((value) => globals.cameras = value);
    _passwordVisible = false;
  }

  @override
  Widget build(BuildContext context) {
    concertModel = new ConcertModel();

    return WillPopScope(
        onWillPop: () async => showDialog<bool>(
              context: context,
              builder: (c) => AlertDialog(
                backgroundColor: Colors.black,
                title: Text(
                  'Attention !',
                  style: TextStyle(
                      color: Colors.red,
                      fontFamily: "Titres",
                      fontSize: MediaQuery.of(context).size.height * 0.15),
                ),
                content: Text(
                  'Voulez vous vraiment quitter ?',
                  style: TextStyle(
                      fontFamily: "Corps",
                      fontSize: MediaQuery.of(context).size.height * 0.1),
                ),
                actions: [
                  TextButton(
                      child: Text(
                        'Oui',
                        style: TextStyle(
                            fontFamily: "Corps",
                            fontSize: MediaQuery.of(context).size.height * 0.1),
                      ),
                      onPressed: () => exit(0)),
                  TextButton(
                    child: Text(
                      'Non',
                      style: TextStyle(
                          color: Colors.red,
                          fontFamily: "Corps",
                          fontSize: MediaQuery.of(context).size.height * 0.1),
                    ),
                    onPressed: () => Navigator.pop(context, false),
                  ),
                ],
              ),
            ),
        child: SafeArea(
          child: Scaffold(
            resizeToAvoidBottomInset: false,
            backgroundColor: Theme.of(context).backgroundColor,
            body: Container(
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  image: DecorationImage(
                image: AssetImage("assets/texture-simple.png"),
                fit: BoxFit.cover,
              )),
              // Center is a layout widget. It takes a single child and positions it
              // in the middle of the parent.
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height * 0.1,
                    child: TextField(
                      decoration: InputDecoration(hintText: 'Nom du concert'),
                      textAlign: TextAlign.center,
                      controller: titleController,
                      style: TextStyle(
                          fontFamily: 'Corps',
                          fontSize: MediaQuery.of(context).size.height * 0.05),
                      onChanged: updateNom(titleController.text),
                    ),
                  ),
                  Container(
                      height: MediaQuery.of(context).size.height * 0.1,
                      child: TextFormField(
                        textAlign: TextAlign.center,
                        keyboardType: TextInputType.text,
                        controller: passwordController,
                        onChanged: updateMdp(passwordController.text),
                        style: TextStyle(
                            fontFamily: 'Corps',
                            fontSize:
                                MediaQuery.of(context).size.height * 0.05),
                        obscureText:
                            !_passwordVisible, //This will obscure text dynamically
                        decoration: InputDecoration(
                          hintText: 'Enter your password',
                          // Here is key idea
                          suffixIcon: IconButton(
                            icon: Icon(
                              // Based on passwordVisible state choose the icon
                              _passwordVisible
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Theme.of(context).primaryColorDark,
                            ),
                            onPressed: () {
                              // Update the state i.e. toogle the state of passwordVisible variable
                              setState(() {
                                _passwordVisible = !_passwordVisible;
                              });
                            },
                          ),
                        ),
                      )),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.4,
                    child: TextField(
                      decoration: InputDecoration(
                        hintText: 'Description',
                      ),
                      minLines: null,
                      maxLines: null,
                      expands: true,
                      textAlign: TextAlign.center,
                      controller: descController,
                      style: TextStyle(
                          fontFamily: 'Corps',
                          fontSize: MediaQuery.of(context).size.height * 0.05),
                      onChanged: updateDesc(descController.text),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.3,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        CreerConcertButton(this.concertModel),
                        RejoindreConcertButton(this.concertModel),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ));
  }
}
