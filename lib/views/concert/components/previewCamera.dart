import 'dart:async';
import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:inside/models/cameras.dart' as globals;

///Widget contenant une preview de la première camera
///
///Liste des cameras dans models/cameras.dart
class CameraApp extends StatefulWidget {
  @override
  _CameraAppState createState() => _CameraAppState();
}

class _CameraAppState extends State<CameraApp> {
  @override
  void initState() {
    super.initState();
    print(globals.cameras);
    //Première camera,resolution maximum, audio activé
    globals.cameraController = CameraController(
        globals.cameras[0], ResolutionPreset.max,
        enableAudio: true);

    //Une fois la caméra initialisée on monte le widget
    globals.cameraController.initialize().then((_) {
      if (!mounted) {
        return;
      }
      //globals.cameraController.unlockCaptureOrientation();
      setState(() {});
    });
  }

  @override
  void dispose() {
    //On libère la camera lors de la suppression du  widget
    globals.cameraController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (!globals.cameraController.value.isInitialized) {
      print('Initialisation camera failed');
      return Container();
    }
    print('Initialisation camera successful');
    return MaterialApp(
      home: CameraPreview(globals.cameraController),
    );
  }
}
