import 'package:communication/communication.dart';
import 'package:flutter/material.dart';

import '../../../models/concert.dart';
import '../../../routes.dart';

import 'package:restart_app/restart_app.dart';

///Bouton permettant de mettre fin au concert
class TerminerConcertButton extends StatelessWidget {
  ConcertModel concertModel;

  TerminerConcertButton(ConcertModel concertModel) {
    this.concertModel = concertModel;
  }

  onClick(BuildContext context) {
    print(concertModel.nom);
    print(concertModel.mdp);
    print(concertModel.description);

    //Envoi de la requête de fin du concert
    Communication.terminerConcert().then((value) {
        //Si la requête s'est bien passée
        print('passage vers la vue menu');
      Restart.restartApp(webOrigin: '/mainMenu');
      /*
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: routes.values.elementAt(0),
              settings:
                  RouteSettings(arguments: concertModel, name: '/mainMenu')),
        );
      */
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      //définition de la taille et du style du container principal
      margin: EdgeInsets.all(5),
      width: MediaQuery.of(context).size.width * 0.25,
      height: MediaQuery.of(context).size.height * 0.20,
      decoration: BoxDecoration(
          border: Border.all(color: Color.fromRGBO(32, 25, 90, 1)),
          borderRadius: BorderRadius.all(Radius.circular(20)),
          boxShadow: [
            BoxShadow(
              spreadRadius: 1.5,
              offset: new Offset(2, 2),
              color: Color.fromRGBO(0, 0, 0, 0.75),
            ),
          ],
          image: DecorationImage(
            image: AssetImage("assets/texture-simple.png"),
            fit: BoxFit.cover,
          )),
      //bouton
      child: TextButton(
        style: TextButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
        ),
        //fonction appelée lors de la pression du bouton
        onPressed: () {
          onClick(context);
        },
        child: Align(
          child: Text('Terminer le \n concert',
              style: TextStyle(
                fontSize: MediaQuery.of(context).size.height * 0.05,
                color: Colors.white,
                fontFamily: 'Corps',
              ),
              textAlign: TextAlign.center),
        ),
      ),
    );
  }
}
