//import 'dart:io';

import 'package:flutter/material.dart';
import 'package:inside/views/concert/components/demarrerConcertButton.dart';
import 'package:inside/views/concert/components/previewCamera.dart';
import 'package:inside/views/concert/components/terminerConcertButton.dart';

import '../../models/concert.dart';
import '../../routes.dart';

///Page de vue Concert
class ConcertPage extends StatelessWidget {
  ConcertModel concertModel;

  @override
  Widget build(BuildContext context) {
    concertModel = ModalRoute.of(context).settings.arguments as ConcertModel;

    return WillPopScope(
        onWillPop: () async => showDialog<bool>(
              context: context,
              builder: (c) => AlertDialog(
                backgroundColor: Colors.black,
                title: Text(
                  'Attention !',
                  style: TextStyle(
                      color: Colors.red,
                      fontFamily: "Titres",
                      fontSize: MediaQuery.of(context).size.height * 0.15),
                ),
                content: Text(
                  'Voulez vous vraiment retourner au menu ?',
                  style: TextStyle(
                      fontFamily: "Corps",
                      fontSize: MediaQuery.of(context).size.height * 0.1),
                ),
                actions: [
                  TextButton(
                      child: Text(
                        'Oui',
                        style: TextStyle(
                            fontFamily: "Corps",
                            fontSize: MediaQuery.of(context).size.height * 0.1),
                      ),
                      onPressed: () => Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(
                              builder: routes.values.elementAt(0),
                              settings: RouteSettings(name: '/mainMenu')),
                          (route) => false)),
                  TextButton(
                    child: Text(
                      'Non',
                      style: TextStyle(
                          color: Colors.red,
                          fontFamily: "Corps",
                          fontSize: MediaQuery.of(context).size.height * 0.1),
                    ),
                    onPressed: () => Navigator.pop(context, false),
                  ),
                ],
              ),
            ),
        child: SafeArea(
            child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Theme.of(context).backgroundColor,
          body: Container(
            alignment: Alignment.center,
            decoration: BoxDecoration(
                image: DecorationImage(
              image: AssetImage("assets/texture-simple.png"),
              fit: BoxFit.cover,
            )),
            // Center is a layout widget. It takes a single child and positions it
            // in the middle of the parent.
            child: Row(children: [
              Container(
                width: MediaQuery.of(context).size.width * 0.73,
                //color: Colors.yellow,
                child: CameraApp(),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  DemarrerConcertButton(concertModel),
                  TerminerConcertButton(concertModel)
                ],
              )
            ]),
          ),
        )));
  }
}
