// Defines every route of the project, in order to navigate between pages
import 'package:inside/views/concert/concertPage.dart';
import 'package:inside/views/mainMenu/mainMenu.dart';

final routes = {
  '/mainMenu': (context) => MainMenu(),
  '/concert': (context) => ConcertPage(),
};
