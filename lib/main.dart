import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:inside/routes.dart';

import 'models/cameras.dart' as globals;

// Landscape mode
void landscapeModeOnly() {
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.landscapeLeft,
    DeviceOrientation.landscapeRight,
  ]);
}

Future<void> main() async {
  //Attendre la fin de l'initialisation des widgets
  WidgetsFlutterBinding.ensureInitialized();
  //Recherche des cameras
  globals.cameras = await availableCameras();

  //Lancement de l'application
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    landscapeModeOnly();
    return MaterialApp(
      title: 'Inside',
      theme: ThemeData(
        accentColor: Color.fromRGBO(32, 25, 90, 1),
        canvasColor: Colors.transparent,
        brightness: Brightness.dark,
        backgroundColor: Color.fromRGBO(160, 133, 87, 1),
        fontFamily: 'Roboto',
      ),
      initialRoute: '/mainMenu',
      routes: routes,
    );
  }
}
