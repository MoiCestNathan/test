library camera.globals;

import 'package:camera/camera.dart';

///Liste des cameras disponibles
List<CameraDescription> cameras;

///Controlleur de la camera utilisée
CameraController cameraController;
