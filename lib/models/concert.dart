///Représente les informations necéssaires à la connexion au concert
class ConcertModel {
  String nom = "";

  String mdp = "";

  String description = "";

  ConcertModel();

  ConcertModel.initDisplay(String nom, String mdp, String description) {
    this.nom = nom;
    this.mdp = mdp;
    this.description = description;
  }
}
